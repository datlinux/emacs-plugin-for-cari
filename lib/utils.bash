#!/usr/bin/env bash

set -euo pipefail

TOOL_NAME="emacs"
TOOL_TEST="emacs"
IS_NEW_OR_UPGR="0"

fail() {
  echo -e "cari-$TOOL_NAME: $*"
  exit 1
}

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

inst_version() {
  if [ ! -f /usr/bin/emacs ]; then
    return 0
  fi
  echo "`apt-cache policy emacs | head -2 | tail -1 | sed -e 's/ //g' | cut -d ":" -f3 | cut -d "+" -f1`"
}

avail_version() {
  echo "`apt-cache policy emacs | head -3 | tail -1 | sed -e 's/ //g' | cut -d ":" -f3 | cut -d "+" -f1`"
}

list_all_versions() {
  iv=`inst_version`
  av=`avail_version`
  if [[ "${iv}" == "${av}"* ]]; then
    echo "$iv"
  else
    echo "$iv"
    echo "$av"
  fi
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"
  local avail_v="`avail_version`"

  if [ "$install_type" != "version" ]; then
    fail "cari-$TOOL_NAME supports release installs only!"
  fi

  (
    echo "Available version: $avail_v"
    if [ -f "/usr/bin/emacs" ]; then
      local emacs_v="`inst_version`"
      echo "Installed version: $emacs_v"
      if [[ ! "$emacs_v" == "$avail_v" ]]; then
        sudo apt upgrade -y --only-upgrade emacs elpa-ess
      fi
    else
      sudo apt install -y emacs elpa-ess
    fi
    mkdir -p "$install_path/bin"
    touch "$install_path/bin/emacs"
    chmod a+x "$install_path/bin/emacs"
    echo "/usr/bin/emacs \"\$@\"" >> $install_path/bin/emacs
    
    test -x "$install_path/bin/$TOOL_TEST" || fail "Expected $install_path/bin/$TOOL_TEST to be executable."
    echo "$TOOL_NAME $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing $TOOL_NAME $version."
  )
}

post_install() {
  rm -rf "$CARI_DOWNLOAD_PATH/"
  if [ -z ${DAT_CUBIC+x} ] && [ -f /usr/share/applications/emacs.desktop ]; then
    sudo rm /usr/share/applications/emacs.desktop
  fi
  exit 0
}
